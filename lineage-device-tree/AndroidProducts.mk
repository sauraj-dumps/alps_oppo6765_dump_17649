#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_RMX2189.mk

COMMON_LUNCH_CHOICES := \
    lineage_RMX2189-user \
    lineage_RMX2189-userdebug \
    lineage_RMX2189-eng
